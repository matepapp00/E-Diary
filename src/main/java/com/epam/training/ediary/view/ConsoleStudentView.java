package com.epam.training.ediary.view;

import com.epam.training.ediary.domain.Course;
import com.epam.training.ediary.domain.Grade;
import com.epam.training.ediary.domain.Student;

import java.util.List;
import java.util.Scanner;

public class ConsoleStudentView implements StudentView {
    public boolean finished=false;
    public static boolean checkRange(Integer number, Integer rangeStart, Integer rangeEnd)
    {
        return number>=rangeStart && number <=rangeEnd;
    }
    @Override
    public void printWelcomeMessage(Student student) {
        System.out.println("Welcome student: "+student.getName()+", your class is: "+student.getSchoolClass().getName());
    }

    @Override
    public void printCourses(List<Course> courses) {
        System.out.println("Your courses:");
        for(int i=0;i<courses.size();++i)
        {
            System.out.println((i+1)+". "+courses.get(i).getSubject());
        }
    }

    @Override
    public Course selectCourse(List<Course> courses) {
        Scanner in = new Scanner(System.in);
        boolean checkOK=false;
        Integer index=0;
        while(!checkOK)
        {
            System.out.println("Choose the number of the course (between: 1-"+courses.size()+")");
            String strindex=in.nextLine();
            try{
                index=Integer.parseInt(strindex);
                checkOK=checkRange(index,1,courses.size());
                if(!checkOK)
                {
                    System.out.println("Input is not in range!");
                }
            }
            catch (Exception e)
            {
                System.out.println("Input should be Integer!");
            }


        }

        return courses.get(index-1);

    }

    @Override
    public void printGrades(List<Grade> grades) {
        Integer size= grades.size();
        System.out.print("Grades: ");

        for(int i=0;i<size;++i)
        {
            if(i==size-1)
            {
                System.out.print(grades.get(i).getValue());
            }
            else
            {
                System.out.print(grades.get(i).getValue()+", ");
            }
        }
        System.out.println();
    }

    @Override
    public boolean isFinished() {
        return finished;
    }
}
