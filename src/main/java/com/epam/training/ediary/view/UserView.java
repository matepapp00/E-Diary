package com.epam.training.ediary.view;

import com.epam.training.ediary.domain.Credentials;

public interface UserView {
    public Credentials readCredentials();
    public void printInvalidCredentials();
}
