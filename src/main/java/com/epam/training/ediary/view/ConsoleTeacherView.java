package com.epam.training.ediary.view;

import com.epam.training.ediary.domain.Course;
import com.epam.training.ediary.domain.Grade;
import com.epam.training.ediary.domain.Student;
import com.epam.training.ediary.domain.Teacher;

import java.util.List;
import java.util.Scanner;

public class ConsoleTeacherView implements TeacherView {
    public boolean finished=false;
    public static boolean checkRange(Integer number, Integer rangeStart, Integer rangeEnd)
    {
        return number>=rangeStart && number <=rangeEnd;
    }
    @Override
    public void printWelcomeMessage(Teacher teacher) {
        System.out.println("Welcome teacher "+teacher.getName());
    }

    @Override
    public void printCourses(List<Course> courses) {
        System.out.println("Your courses:");
        for(int i=0;i<courses.size();++i)
        {
            System.out.println((i+1)+". "+courses.get(i).getSubject());
        }
    }

    @Override
    public Course selectCourse(List<Course> courses) {
        Scanner in = new Scanner(System.in);
        boolean checkOK=false;
        Integer index=0;
        while(!checkOK)
        {
            System.out.println("Choose the number of the course (between: 1-"+courses.size()+")");
            String strindex=in.nextLine();
            try{
                index=Integer.parseInt(strindex);
                checkOK=checkRange(index,1,courses.size());
                if(!checkOK)
                {
                    System.out.println("Input is not in range!");
                }
            }
            catch (Exception e)
            {
                System.out.println("Input should be Integer!");
            }


        }

        return courses.get(index-1);
    }

    @Override
    public void printStudents(List<Student> students) {
        System.out.println("The students of this course are: ");
        for(int i=0;i<students.size();++i)
        {
            System.out.println((i+1)+". "+students.get(i).getName());
        }
    }

    @Override
    public Student selectStudent(List<Student> students)   {

        Scanner in = new Scanner(System.in);
        boolean checkOK=false;
        Integer index=0;
        while(!checkOK)
        {
            System.out.println("Choose the number of the student (between: 1-"+students.size()+")");
            String strindex=in.nextLine();
            try{
                index=Integer.parseInt(strindex);
                checkOK=checkRange(index,1,students.size());
                if(!checkOK)
                {
                    System.out.println("Input is not in range!");
                }
            }
            catch (Exception e)
            {
                System.out.println("Input should be Integer!");
            }


        }
        return students.get(index-1);
    }

    @Override
    public Integer readGradeValue() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the grade value:");
        String gradeString=in.nextLine();
        return Integer.parseInt(gradeString);
    }

    @Override
    public void printGrade(Grade grade) {
        System.out.println("Grade created: "+grade.toString());
    }
}
