package com.epam.training.ediary.view;

import com.epam.training.ediary.domain.Course;
import com.epam.training.ediary.domain.Grade;
import com.epam.training.ediary.domain.Student;
import com.epam.training.ediary.domain.Teacher;

import java.util.List;

public interface TeacherView {
    public void printWelcomeMessage(Teacher teacher);
    public void printCourses(List<Course> courses);
    public Course selectCourse(List<Course> courses);
    public void printStudents(List<Student> students);
    public Student selectStudent(List<Student> students);
    public Integer readGradeValue();
    public void printGrade(Grade grade);
}
