package com.epam.training.ediary.service;

import com.epam.training.ediary.domain.Credentials;
import com.epam.training.ediary.domain.User;

public interface UserService {
    public User authenticate(Credentials credentials);
}
