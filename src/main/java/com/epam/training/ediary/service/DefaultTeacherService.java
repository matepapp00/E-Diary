package com.epam.training.ediary.service;

import com.epam.training.ediary.domain.Course;
import com.epam.training.ediary.domain.Grade;
import com.epam.training.ediary.persistence.DataStore;
import com.epam.training.ediary.domain.Student;
import com.epam.training.ediary.domain.Teacher;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DefaultTeacherService implements TeacherService {
    private DataStore dataStore;
    public DefaultTeacherService(DataStore pdatastore)
    {
        dataStore=pdatastore;
    }

    @Override
    public List<Course> findCoursesByTeacher(Teacher teacher) throws java.lang.IllegalArgumentException{
        List<Course> coursesByStudents= new ArrayList<>();

        for(Course course : dataStore.getCourses()) {
            if(teacher.getId().equals(course.getTeacher().getId())){
                coursesByStudents.add(course);
            }
        }
        if(coursesByStudents.isEmpty())
        {
            throw new IllegalArgumentException("kurzusok uresek!");
        }
        return coursesByStudents;

    }

    @Override
    public List<Student> getStudentsFromCourse(Course course) throws java.lang.IllegalArgumentException{
        List<Student> students= new ArrayList<>();


        for(var student : dataStore.getStudents()) {
            if(student.getSchoolClass().getName().equals(course.getSchoolClass().getName())){
                students.add(student);
            }
        }
        if(students.isEmpty())
        {
            throw new IllegalArgumentException("illegal argument");
        }
        return students;
    }

    @Override
    public Grade createGrade(Student student, Course course, int value) throws java.lang.IllegalArgumentException{

        List<Student> studs = getStudentsFromCourse(course);
        boolean found=false;
        for(Student s : studs)
        {
            if(s.getId().equals(student.getId()))
            {
                found=true;
                break;
            }
        }
        if(!found)
        {
            throw new IllegalArgumentException("illegal argument");
        }
        Grade grade= new Grade();
        grade.setValue(value);
        grade.setStudent(student);
        grade.setCourse(course);
        LocalDate date = LocalDate.now();
        DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd.");
        grade.setDate(LocalDate.parse(date.toString().replace('-','.')+".",DATEFORMATTER));
        dataStore.saveGrade(grade);
        return grade;
    }
}
