package com.epam.training.ediary.service;

import com.epam.training.ediary.domain.Course;
import com.epam.training.ediary.domain.Grade;
import com.epam.training.ediary.domain.Student;
import com.epam.training.ediary.domain.Teacher;

import java.util.List;

public interface TeacherService {
    public List<Course> findCoursesByTeacher(Teacher teacher);
    public List<Student> getStudentsFromCourse(Course course);
    public Grade createGrade(Student student,Course course,int value);
}
