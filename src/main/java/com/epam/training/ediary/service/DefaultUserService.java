package com.epam.training.ediary.service;

import com.epam.training.ediary.domain.Credentials;
import com.epam.training.ediary.domain.Student;
import com.epam.training.ediary.domain.Teacher;
import com.epam.training.ediary.persistence.DataStore;
import com.epam.training.ediary.domain.User;

import java.util.List;
import java.util.Optional;

public class DefaultUserService implements UserService {
    private DataStore dataStore;
    public DefaultUserService(DataStore pdatastore)
    {
        dataStore=pdatastore;
    }
    @Override
    public User authenticate(Credentials credentials)throws AuthenticationException {

        List<Student> students = dataStore.getStudents();
        List<Teacher> teachers = dataStore.getTeachers();
        String password = credentials.getPassword();
        String loginName = credentials.getLoginName();
        Optional<Student> optionalStudent = students.stream()
                .filter(x ->
                        (password.equals(x.getCredentials().getPassword())
                        && loginName.equals(x.getCredentials().getLoginName())))
                .findFirst();
        Optional<Teacher> optionalTeacher = teachers.stream()
                .filter(x ->
                        (password.equals(x.getCredentials().getPassword())
                                && loginName.equals(x.getCredentials().getLoginName())))
                .findFirst();

        if(optionalStudent.isPresent()) {
            return optionalStudent.get();
        }
        else if(optionalTeacher.isPresent())
        {
            return optionalTeacher.get();
        }
        throw new AuthenticationException();


    }
}
