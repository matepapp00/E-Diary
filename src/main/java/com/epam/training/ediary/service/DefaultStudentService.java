package com.epam.training.ediary.service;

import com.epam.training.ediary.domain.*;
import com.epam.training.ediary.domain.Grade;
import com.epam.training.ediary.persistence.DataStore;
import com.epam.training.ediary.domain.Student;

import java.util.ArrayList;
import java.util.List;

public class DefaultStudentService implements StudentService {
    private DataStore dataStore;
    public DefaultStudentService(DataStore pdatastore)
    {
        dataStore=pdatastore;
    }

    @Override
    public List<Course> findCoursesByStudent(Student student) throws java.lang.IllegalArgumentException {
        List<Course> coursesByStudents= new ArrayList<>();

        for(Course course : dataStore.getCourses())
        {
            if(student.getSchoolClass().getName().equals(course.getSchoolClass().getName()))
            {
                coursesByStudents.add(course);
            }
        }
        return coursesByStudents;
    }

    @Override
    public List<Grade> findGradesByStudentAndCourse(Student student, Course course) throws java.lang.IllegalArgumentException{
        List<Grade> gradesByStudent= new ArrayList<>();


        try {
            for (Grade grade : dataStore.getGrades()) {
                boolean logic1 = grade.getStudent().equals(student);
                boolean logic2 = grade.getCourse().equals(course);
                if (logic1 && logic2) {
                    gradesByStudent.add(grade);
                }
            }

        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("bad");
        }
        if(gradesByStudent.isEmpty())
        {
            throw new IllegalArgumentException("good");
        }
        return gradesByStudent;
    }


}
