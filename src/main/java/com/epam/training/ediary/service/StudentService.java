package com.epam.training.ediary.service;

import com.epam.training.ediary.domain.Course;
import com.epam.training.ediary.domain.Grade;
import com.epam.training.ediary.domain.Student;

import java.util.List;

public interface StudentService {
    public List<Course> findCoursesByStudent(Student student);
    public List<Grade> findGradesByStudentAndCourse(Student student,Course course) throws java.lang.IllegalArgumentException;
}
