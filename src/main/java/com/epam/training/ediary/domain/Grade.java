package com.epam.training.ediary.domain;
import com.opencsv.bean.CsvBindByPosition;

import java.time.LocalDate;

public class Grade
{
    @CsvBindByPosition(position = 1)
    private int value;
    @CsvBindByPosition(position = 3)
    private LocalDate date;
    @CsvBindByPosition(position = 2)
    private Course course;
    @CsvBindByPosition(position = 0)
    private Student student;
    public Grade(){}
    public Grade(int value, LocalDate date, Course course, Student student) {
        this.value = value;
        this.date = date;
        this.course = course;
        this.student = student;
    }

    public int getValue(){
        return value;
    }
    public void setValue(int pValue){
        value=pValue;
    }
    public LocalDate getDate(){
        return date;
    }
    public void setDate(LocalDate pDate){
        date=pDate;
    }
    public Course getCourse(){
        return course;
    }
    public void setCourse(Course pCourse){
        course=pCourse;
    }
    public Student getStudent(){
        return student;
    }
    public void setStudent(Student pStudent){
        student=pStudent;
    }
    @Override
    public String toString() {
        return "Grade: "+this.value+", Student: "+ this.student.toString()+", Date: "+ this.date.toString();
    }
    public String convertValue() {
        return Integer.toString(value);
    }

    public String convertId() {
        return Long.toString(student.getId());
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof Grade)) {
            return false;
        }
        Grade castedGrade = (Grade) o;

        return castedGrade.getValue()==value
                && castedGrade.getStudent().equals(student)
                && castedGrade.getDate().equals(date)
                && castedGrade.getCourse().equals(course);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + student.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + value;
        result = 31 * result + course.hashCode();
        return result;
    }

}
