package com.epam.training.ediary.domain;

public class SchoolClass
{
    private String name;
    public SchoolClass(){}
    public SchoolClass(String name) {
        this.name = name;
    }

    public String getName(){
        return  name;
    }
    public void setName(String pName){
        name=pName;
    }
    @Override
    public String toString() {
        return this.name;
    }
    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof SchoolClass)) {
            return false;
        }
        SchoolClass castedSchoolClass = (SchoolClass) o;

        return castedSchoolClass.getName().equals(this.name);
    }
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        return result;
    }
}
