package com.epam.training.ediary.domain;

public class Student extends User
{
    private SchoolClass schoolClass;
    public Student(){}
    public Student(SchoolClass schoolClass) {
        super();
        this.schoolClass = schoolClass;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }
    public void setSchoolClass(SchoolClass pSchoolClass) {
        schoolClass = pSchoolClass;
    }

    @Override
    public String toString() {
        return "Name: "+this.getName();
    }
    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof Student)) {
            return false;
        }
        Student castedStudent = (Student) o;

         boolean a=castedStudent.getCredentials().equals(getCredentials());
        boolean b=castedStudent.getId()==getId();
        boolean c=castedStudent.getName().equals(getName());
        boolean d=castedStudent.getSchoolClass().equals(getSchoolClass());
        return a &&b && c&& d;
    }
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + schoolClass.hashCode();
        result = 31 * result + getCredentials().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getId().intValue();
        return result;
    }
}
