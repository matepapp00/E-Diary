package com.epam.training.ediary.domain;

import com.opencsv.bean.CsvBindByPosition;

public class Credentials {
    @CsvBindByPosition(position = 1)
    private String loginName;
    @CsvBindByPosition(position = 2)
    private String password;

    public Credentials(){}
    public Credentials(String loginName, String password) {
        this.loginName = loginName;
        this.password = password;
    }

    public String getLoginName() {
        return loginName;
    }
    public void setLoginName(String ploginName) {
        loginName=ploginName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String pPassword) {
        password=pPassword;
    }

    @Override
    public String toString() {
        return this.loginName+" "+this.password;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof Credentials)) {
            return false;
        }
        Credentials castedCredentials = (Credentials) o;

        return castedCredentials.getPassword().equals(password)
                && castedCredentials.getLoginName().equals(loginName);
    }
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + password.hashCode();
        result = 31 * result + loginName.hashCode();
        return result;
    }
}
