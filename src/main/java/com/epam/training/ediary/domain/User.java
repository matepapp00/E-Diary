package com.epam.training.ediary.domain;

public class User
{
    private long id;
    private String name;
    private Credentials credentials;
    public User(){}
    public User(long id, String name, Credentials credentials) {
        this.id = id;
        this.name = name;
        this.credentials = credentials;
    }

    public Long getId(){
        return id;
    }
    public void setId(long pId){
        id=pId;
    }
    public String getName(){
        return name;
    }
    public void setName(String pName){
        name=pName;
    }
    public Credentials getCredentials(){
        return credentials;
    }
    public void setCredentials(Credentials pCredentials){
        credentials=pCredentials;
    }
    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof User)) {
            return false;
        }
        User castedUser = (User) o;

        return castedUser.getId()==id
                && castedUser.getName().equals(name)
                && castedUser.getCredentials().equals(credentials);
    }
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + getCredentials().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + (int)(getId() ^ (getId() >>> 32));
        return result;
    }
}
