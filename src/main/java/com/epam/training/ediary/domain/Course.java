package com.epam.training.ediary.domain;

import com.opencsv.bean.CsvBindByPosition;

public class Course
{
    @CsvBindByPosition(position = 0)
    private Subject subject;
    @CsvBindByPosition(position = 1)
    private Teacher teacher;
    @CsvBindByPosition(position = 2)
    private SchoolClass schoolClass;
    public Course(){}
    public Course(Subject subject, Teacher teacher, SchoolClass schoolClass) {
        this.subject = subject;
        this.teacher = teacher;
        this.schoolClass = schoolClass;
    }

    public Subject getSubject() {
        return subject;
    }
    public void setSubject(Subject pSubject) {
        subject = pSubject;
    }
    public Teacher getTeacher() {
        return teacher;
    }
    public void setTeacher(Teacher pTeacher) {
        teacher = pTeacher;
    }
    public SchoolClass getSchoolClass() {
        return schoolClass;
    }
    public void setSchoolClass(SchoolClass pSchoolClass) {
        schoolClass = pSchoolClass;
    }

    public String convertSubjectToString() {
        String subjectValue="";
        switch (subject){
            case MATH -> {
                subjectValue="MATH";
                break;
            }
            case HISTORY -> {
                subjectValue="HISTORY";
                break;
            }
            case PHYSICS -> {
                subjectValue="PHYSICS";
                break;
            }
            case BIOLOGY -> {
                subjectValue="BIOLOGY";
                break;
            }
            case CHEMISTRY -> {
                subjectValue="CHEMISTRY";
                break;
            }
            case COMPUTER_SCIENCE -> {
                subjectValue="COMPUTER_SCIENCE";
                break;
            }
        }
        return  subjectValue;
    }

    public Subject convertStringToSubject(String sub) {
        Subject psubject=null;
        switch (sub){
            case "MATH" -> {
                psubject=Subject.MATH;
                break;
            }
            case "HISTORY" -> {
                psubject=Subject.HISTORY;
                break;
            }
            case "PHYSICS" -> {
                psubject=Subject.PHYSICS;
                break;
            }
            case "BIOLOGY" -> {
                psubject=Subject.BIOLOGY;
                break;
            }
            case "CHEMISTRY" -> {
                psubject=Subject.CHEMISTRY;
                break;
            }
            case "COMPUTER_SCIENCE" -> {
                psubject=Subject.COMPUTER_SCIENCE;
                break;
            }
        }
        return psubject;
    }
    public Integer convertSubjectToInteger() {
        Integer value = null;
        switch (subject){
            case MATH -> {
                value=0;
                break;
            }
            case HISTORY -> {
                value=1;
                break;
            }
            case PHYSICS -> {
                value=2;
                break;
            }
            case BIOLOGY -> {
                value=3;
                break;
            }
            case CHEMISTRY -> {
                value=4;
                break;
            }
            case COMPUTER_SCIENCE -> {
                value=5;
                break;
            }
        }
        return value;
    }

    public Subject convertIntToSubject(int value) {
        subject=subject.DEFAULT;
        switch (value){
            case 0 -> {
                subject=subject.MATH;
                break;
            }
            case 1 -> {
                subject=subject.HISTORY;
                break;
            }
            case 2 -> {
                subject=subject.PHYSICS;
                break;
            }
            case 3 -> {
                subject=subject.BIOLOGY;
                break;
            }
            case 4 -> {
                subject=subject.CHEMISTRY;
                break;
            }
            case 5 -> {
                subject=subject.COMPUTER_SCIENCE;
                break;
            }
        }
        return subject;
    }

    @Override
    public String toString() {
        return "Name: "+convertSubjectToString()+" School class: "+this.schoolClass;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof Course)) {
            return false;
        }
        Course castedCourse= (Course) o;

        return castedCourse.getSubject().equals(subject)
                && castedCourse.getTeacher().equals(teacher)
                && castedCourse.getSchoolClass().equals(schoolClass);
    }
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + subject.hashCode();
        result = 31 * result + teacher.hashCode();
        result = 31 * result + schoolClass.hashCode();
        return result;
    }
}
