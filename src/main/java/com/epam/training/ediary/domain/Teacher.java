package com.epam.training.ediary.domain;

public class Teacher extends User
{
    public Teacher() {
        super();
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof Teacher)) {
            return false;
        }
        Teacher castedTeacher = (Teacher) o;

        return castedTeacher.getId().equals(this.getId()) && castedTeacher.getName().equals(getName()) && castedTeacher.getCredentials().equals(getCredentials());
    }
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + getCredentials().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getId().intValue();
        return result;
    }
}
