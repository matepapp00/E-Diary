package com.epam.training.ediary.persistence;

import com.epam.training.ediary.domain.*;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DefaultDataStore implements DataStore {
    private List<Student> students;
    private List<Teacher> teachers;
    private List<Course> courses;
    private List<Grade> grades;
    private String baseDirPath;

    public String returnAdequatePathInStringForCSV(String fileName) {
        return baseDirPath+fileName;
    }

    public Student assembleStudent(String Id,String loginName,String password,String name,String schoolClass) {
        Student currStudent=new Student();
        currStudent.setId(Long.parseLong(Id));
        SchoolClass currStudentSchoolClass=new SchoolClass();
        Credentials currStudentCreds=new Credentials();
        currStudentCreds.setLoginName(loginName);
        currStudentCreds.setPassword(password);
        currStudent.setName(name);
        currStudent.setCredentials(currStudentCreds);
        currStudentSchoolClass.setName(schoolClass);
        currStudent.setSchoolClass(currStudentSchoolClass);
        return currStudent;
    }

    public Teacher assembleTeacher(String Id,String loginName,String password,String name) {
        Teacher currTeacher = new Teacher();
        Credentials currTeacherCreds=new Credentials();
        currTeacher.setId(Long.parseLong(Id));
        currTeacherCreds.setLoginName(loginName);
        currTeacherCreds.setPassword(password);
        currTeacher.setName(name);
        currTeacher.setCredentials(currTeacherCreds);
        return currTeacher;
    }

    public Grade assembleGrade(String Id,String Value,String CourseName,String Date) throws IllegalArgumentException{
        Grade currGrade=new Grade();
        Student currGradeStudent=new Student();
        Course currGradeCourse= new Course();
        currGradeStudent.setId(Long.parseLong(Id));
        List<Student> students = getStudents();
        List<Course> courses =getCourses();
        List<Course> coursesByStudent=new ArrayList<>();
        for(Student student : students)
        {
            if(student.getId()==Long.parseLong(Id))
            {
                currGrade.setStudent(student);
                break;
            }
        }
        if(currGrade.getStudent()==null)
        {
            throw new IllegalArgumentException("Illegal argument");
        }
        for(Course course : courses) {
            if(currGrade.getStudent().getSchoolClass().equals(course.getSchoolClass())){
                coursesByStudent.add(course);
            }
        }
        for(Course course : coursesByStudent) {
            if(CourseName.equals(course.getSubject().toString())){
                currGrade.setCourse(course);
                break;
            }
        }

        currGrade.setValue(Integer.parseInt(Value));

        //currGradeCourse.setSubject(currGradeCourse.convertStringToSubject(CourseName));
        DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd.");
        //currGrade.setCourse(currGradeCourse);
        currGrade.setDate(LocalDate.parse(Date,DATEFORMATTER));
        return currGrade;
    }

    public Course assembleCourse(String className, String subjectName,String teacherId){
        Course currCourse = new Course();
        SchoolClass schoolClass = new SchoolClass();
        Teacher currCourseTeacher = new Teacher();
        schoolClass.setName(className);
        currCourse.setSchoolClass(schoolClass);
        currCourse.setSubject(currCourse.convertStringToSubject(subjectName));
        currCourseTeacher.setId(Long.parseLong(teacherId));
        List<Teacher> teachers =getTeachers();
        for(Teacher t : teachers)
        {
            if(t.getId()==Long.parseLong(teacherId))
            {
                currCourse.setTeacher(t);
                break;
            }
        }
        return currCourse;

    }

    public List<Student> getAllStudentsFromCSVAtOnce(String file) {
        List<Student> allStudents= new ArrayList<>();
        try {

            FileReader filereader = new FileReader(file);

            CSVReader csvReader = new CSVReader(filereader);
            List<String[]> allData = csvReader.readAll();

            for (String[] row : allData) {
                Student currStudent=assembleStudent(row[0],row[1],row[2],row[3],row[4]);

                allStudents.add(currStudent);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return allStudents;
    }

    public List<Teacher> getAllTeachersFromCSVAtOnce(String file) {
        List<Teacher> allTeachers= new ArrayList<>();
        try {
            FileReader filereader = new FileReader(file);

            CSVReader csvReader = new CSVReader(filereader);
            List<String[]> allData = csvReader.readAll();

            for (String[] row : allData) {

                Credentials currTeacherCreds=new Credentials();
                Teacher currTeacher=assembleTeacher(row[0],row[1],row[2],row[3]);
                allTeachers.add(currTeacher);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return allTeachers;
    }

    public List<Grade> getAllGradesFromCSVAtOnce(String file) {
        List<Grade> allGrades= new ArrayList<>();
        try {
            FileReader filereader = new FileReader(file);
            CSVReader csvReader = new CSVReader(filereader);

            List<String[]> allData = csvReader.readAll();

            for (String[] row : allData) {
                Grade currGrade=assembleGrade(row[0],row[1],row[2],row[3]);
                allGrades.add(currGrade);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return allGrades;
    }

    public List<Course> getAllCoursesFromCSVAtOnce(String file) {
        List<Course> allCourses= new ArrayList<>();
        try {
            FileReader filereader = new FileReader(file);

            CSVReader csvReader = new CSVReader(filereader);
            List<String[]> allData = csvReader.readAll();

            for (String[] row : allData) {
                Course currCourse = assembleCourse(row[0],row[1],row[2]);

                allCourses.add(currCourse);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return allCourses;
    }

    public void writeNewGradeToCSV(String filePath,Grade grade) {

        File file = new File(filePath);
        try {
            FileWriter outputfile = new FileWriter(file,true);

            CSVWriter writer = new CSVWriter(outputfile);

            String[] gradeData = {grade.convertId(),grade.convertValue(), grade.getCourse().getSubject().toString(),grade.getDate().toString().replace('-','.')+"."};
            writer.writeNext(gradeData);
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        String teacherCSVPath=returnAdequatePathInStringForCSV("teacher.csv");
        String studentCSVPath=returnAdequatePathInStringForCSV("student.csv");
        String courseCSVPath=returnAdequatePathInStringForCSV("course.csv");
        String gradeCSVPath=returnAdequatePathInStringForCSV("grade.csv");
        students=getAllStudentsFromCSVAtOnce(studentCSVPath);
        teachers=getAllTeachersFromCSVAtOnce(teacherCSVPath);
        courses=getAllCoursesFromCSVAtOnce(courseCSVPath);
        grades=getAllGradesFromCSVAtOnce(gradeCSVPath);
    }

    @Override
    public List<Student> getStudents() {
        return students;
    }

    @Override
    public List<Teacher> getTeachers() {

        return teachers;
    }

    @Override
    public List<Course> getCourses() {
        return courses;
    }

    @Override
    public List<Grade> getGrades() {
        return grades;
    }

    @Override
    public void saveGrade(Grade grade) {
        String gradeCSVPath=returnAdequatePathInStringForCSV("grade.csv");
        writeNewGradeToCSV(gradeCSVPath,grade);
        grades.add(grade);
    }

    public DefaultDataStore(String pbaseDirPath) {
        baseDirPath=pbaseDirPath;
    }
}
