package com.epam.training.ediary;

import com.epam.training.ediary.domain.*;
import com.epam.training.ediary.persistence.DefaultDataStore;
import com.epam.training.ediary.service.AuthenticationException;
import com.epam.training.ediary.service.DefaultTeacherService;
import com.epam.training.ediary.service.DefaultStudentService;
import com.epam.training.ediary.service.DefaultUserService;
import com.epam.training.ediary.view.ConsoleStudentView;
import com.epam.training.ediary.view.ConsoleTeacherView;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class Application {
    enum USER_TYPE{UNDEFINED,TEACHER,STUDENT};
    public static void Login(Credentials credentials,Scanner in)
    {
        System.out.println("Login name: ");
        String name=in.nextLine();
        credentials.setLoginName(name);
        System.out.println("Password: ");
        String pwd=in.nextLine();
        credentials.setPassword(pwd);
    }
    public static boolean checkRange(Integer number, Integer rangeStart, Integer rangeEnd)
    {
        return number>=rangeStart && number <=rangeEnd;
    }
    static List<Course> ListCourses(User loginedUser, DefaultTeacherService teacherService, ConsoleTeacherView cTeacherV) throws java.lang.IllegalArgumentException {
        List<Course> courses = teacherService.findCoursesByTeacher((Teacher)loginedUser);
        cTeacherV.printCourses(courses);
        return courses;
    }
    static List<Course> ListCourses(User loginedUser, DefaultStudentService studentService, ConsoleStudentView cStudentV) {
        List<Course> courses = studentService.findCoursesByStudent((Student)loginedUser);
        cStudentV.printCourses(courses);
        return courses;
    }
    static ImmutablePair<List<Student>,Course> ChooseCourse(User loginedUser, DefaultDataStore dataStore, DefaultTeacherService teacherService, Scanner in, List<Course> courses, ConsoleTeacherView cTeacherV)
    {
        Course course = cTeacherV.selectCourse(courses);
        List<Student> students = teacherService.getStudentsFromCourse(course);
        cTeacherV.printStudents(students);
        return new ImmutablePair<List<Student>,Course>(students,course);
    }
    static List<Grade> ListGrades(User loginedUser,List<Course> courses,DefaultStudentService studentService, ConsoleStudentView cStudentV) throws java.lang.IllegalArgumentException
    {
        Scanner in = new Scanner(System.in);
        if(courses.isEmpty())
        {
            throw new IllegalArgumentException("Illegal argument");
        }
        Integer index=0;
        boolean checkOK=false;
        while(!checkOK)
        {
            System.out.println("Choose the number of the course (between: 1-"+courses.size()+")");
            String strindex=in.nextLine();
            try{
                index=Integer.parseInt(strindex);
                checkOK=checkRange(index,1,courses.size());
                if(!checkOK)
                {
                    System.out.println("Input is not in range!");
                }
            }
            catch (Exception e)
            {
                System.out.println("Input should be Integer!");
            }


        }
        Course course = courses.get(index-1);
        List<Grade> grades = studentService.findGradesByStudentAndCourse((Student)loginedUser,course);
        return grades;
    }

    static void ChooseStudent(User loginedUser, DefaultDataStore dataStore, DefaultTeacherService teacherService, Scanner in, ImmutablePair<List<Student>,Course> studentsWithCourse, ConsoleTeacherView cTeacherV)
    {
        List<Student> students = studentsWithCourse.getLeft();
        Course course = studentsWithCourse.getRight();
        Student student = cTeacherV.selectStudent(students);
        Integer gradeValue = cTeacherV.readGradeValue();
        teacherService.createGrade(student,course,gradeValue);
        Grade grade= new Grade();
        grade.setValue(gradeValue);
        grade.setStudent(student);
        grade.setCourse(course);
        //date variable
        LocalDate date = LocalDate.now();
        DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd.");
        grade.setDate(LocalDate.parse(date.toString().replace('-','.')+".",DATEFORMATTER));
        cTeacherV.printGrade(grade);
    }

    public static void main(String[] args) {

        DefaultDataStore dataStore= new DefaultDataStore("input/");
        dataStore.init();
        Scanner in = new Scanner(System.in);
        DefaultUserService userService= new DefaultUserService(dataStore);
        boolean loginFailed=true;
        DefaultTeacherService teacherService = new DefaultTeacherService(dataStore);
        DefaultStudentService studentService = new DefaultStudentService(dataStore);
        ConsoleTeacherView cTeacherV=new ConsoleTeacherView();
        ConsoleStudentView cStudentV=new ConsoleStudentView();
        try
        {

            Credentials loginCred = new Credentials();
            Login(loginCred,in);

            User loginedUser= userService.authenticate(loginCred);
            USER_TYPE userType = loginedUser instanceof Teacher ? USER_TYPE.TEACHER : USER_TYPE.STUDENT;
            switch (userType)
            {
                case TEACHER:
                {
                    cTeacherV.printWelcomeMessage((Teacher)loginedUser);
                    List<Course> courses= ListCourses(loginedUser,teacherService,cTeacherV);
                    ImmutablePair<List<Student>,Course> studentsWithCourse = ChooseCourse(loginedUser,dataStore,teacherService,in,courses,cTeacherV);
                    ChooseStudent(loginedUser,dataStore,teacherService,in,studentsWithCourse,cTeacherV);
                    break;
                }
                case STUDENT:
                {
                    cStudentV.printWelcomeMessage((Student)loginedUser);
                    boolean finished=false;
                    while(!finished) {
                        List<Course> courses = ListCourses(loginedUser, studentService, cStudentV);
                        List<Grade> grades = ListGrades(loginedUser, courses, studentService, cStudentV);
                        if (grades.isEmpty())
                        {
                            finished = true;
                            return;
                        }
                        cStudentV.printGrades(grades);
                        System.out.println("Are you finished? (type y/n):");
                        String ans=in.nextLine();
                        if("y".equals(ans) || "Y".equals(ans))
                        {
                            cStudentV.finished=true;
                            finished=true;
                        }
                    }
                    break;
                }

                default:
                {
                    break;
                }
            }

        }
        catch(AuthenticationException e)
        {

            System.out.println("-> Task :Application.main()\n" +
                    "Login name: incorrect\n" +
                    "Password: incorrect\n" +
                    "Login name and/or password not valid\n");

            System.out.println("> Task :Application.main() FAILED\n\n" +
                    "Execution failed for task ':Application.main()'.\n" +
                    "> Process 'command '"+ System.getProperty("java.home")+"\\bin\\java.exe'' finished with non-zero exit value 1");

        }
    }

}
